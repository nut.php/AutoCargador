**Licencia pública internacional de Creative Commons Reconocimiento-CompartirIgual 4.0**

Mediante el ejercicio de los derechos de licencia (definido a continuación), usted acepta y acuerda estar obligado por los términos y condiciones de esta licencia Creative Commons Atribución-CompartirIgual 4.0 internacional público (&quot;Public License&quot;). En la medida de que esta licencia pública puede ser interpretada como un contrato, se le concede los derechos de licencia en la consideración de su aceptación de estos términos y condiciones, y el licenciante le otorga dichos derechos teniendo en cuenta los beneficios que el licenciante reciba de disponer el Material licenciado bajo estos términos y condiciones.

**Sección 1 – definiciones.**

1. **Adaptado de Material****  **significa material sujeto a derechos de autor y derechos similares que es derivado o basado en el Material con licencia y en que el Material licenciado es traducido, alterado, arreglado, transformado o modificado de otra forma en una manera que requiere permiso bajo derechos de autor y derechos similares por el licenciante. Para los propósitos de esta licencia pública, donde el Material con licencia es una obra musical, el rendimiento o la grabación de sonido, Material adaptado siempre se produce donde el Material con licencia es sincronizar en relación cronometrada con una imagen en movimiento.
2. **Licencia del adaptador****  **significa que la licencia se aplica a su Copyright y derechos similares en sus contribuciones al Material adaptado con arreglo a los términos y condiciones de esta licencia pública.
3. **Compatible con licencia BY-SA****  **significa una licencia enumerada en org/compatiblelicenses, aprobada por Creative Commons como esencialmente equivalente a esta licencia pública.
4. **Derechos de autor y derechos similares**** ,** los derechos de autor o similares relacionados con derechos de autor incluyendo, sin limitación, rendimiento, transmisión, grabación de sonido y Sui Generis la base de datos, los derechos, sin importar cómo los derechos son etiquetados o clasificados. Para los propósitos de esta licencia pública, los derechos especificados en la sección 2(b)(1)-(2) no son derechos de autor y derechos similares.
5. **Medidas tecnológicas efectivas****  **significa que las medidas que, en ausencia de autoridad adecuada, pueden no eludirse en cumplimiento de las obligaciones en el artículo 11 del Tratado de Copyright WIPO adoptado el 20 de diciembre de 1996, leyes o acuerdos internacionales similares.
6. **Las excepciones y limitaciones****  **significan uso justo, trato justo, o cualquier otra excepción o limitación al derecho de autor y derechos similares que se aplica a su uso de los materiales licenciados.
7. **Elementos de licencia****  **significa que los atributos de licencia aparecen en el nombre de una licencia pública de Creative Commons. Los elementos de la licencia de esta licencia pública son atribución y compartir.
8. **Licencia de Material****  **significa que la obra artística o literaria, la base de datos u otro material al que el licenciante aplica esta licencia pública.
9. **El derecho de licencia significa** que los derechos concedidos a usted conforme a los términos y condiciones de esta licencia pública, que se limitan a todos los derechos de autor y derechos similares que se aplican a su uso del Material con licencia y que el licenciante tenga autorización para licencia de.
10. **Otorgante de la licencia****  **significa el individuo o la concesión de derechos bajo esta licencia pública de entidad(es).
11. **Compartir****  **medios para proporcionar material al público por cualquier medio o proceso que requiere el permiso bajo los derechos de licencia, como la reproducción, exhibición pública, representación pública, distribución, difusión, comunicación o importación, y a disposición material público incluyendo en maneras que el miembro del público puede acceder al material desde el lugar y a la vez cada uno de ellos elija.
12. **Sui Generis la base de datos derechos****  **derechos que copyright resultante de la Directiva 96/9/CE del Parlamento Europeo y del Consejo, de 11 de marzo de 1996 sobre la protección jurídica de bases de datos, como enmendado o tuvo éxito, así como otros derechos esencialmente equivalentes en todo el mundo.
13. **Se**** : **el individuo o entidad ejerciendo los derechos con licencia bajo esta licencia pública. ** Su **** ** tiene un significado correspondiente.

**Sección 2 – ámbito de aplicación.**

1. **Concesión de la licencia**.
  1. Sujeto a los términos y condiciones de esta licencia pública, el licenciante por este medio le concede una licencia mundial, libre de regalías, no sublicenciable, no exclusiva, irrevocable para ejercer los derechos de licencia en el Material bajo licencia a:
    1. reproducir y compartir el Material con licencia, en todo o en parte; y
    2. producir, reproducir y compartir Material adaptado.
  2. Las excepciones y limitaciones. Para evitar dudas, donde las excepciones y limitaciones se aplican a su uso, no se aplica esta licencia pública, y no necesita cumplir con los términos y condiciones.
  3. Plazo. El término de esta licencia pública se especifica en la sección 6.
  4. Los medios y formatos; modificaciones técnicas permitidas. El licenciante autoriza a ejercer los derechos de licencia en todos los medios y formatos ahora conocido o creado en lo sucesivo y a realizar modificaciones técnicas necesarias para hacerlo. El Licenciante renuncia y conviene en no aseverar ningún derecho ni autoridad para prohibirlo de hacer modificaciones técnicas necesarias para ejercer los derechos de licencia, incluyendo modificaciones técnicas necesarias para eludir medidas tecnológicas efectivas. Para los propósitos de esta licencia pública, no simplemente hacer modificaciones autorizadas por esta sección 2(a)(4) produce Material adaptado.
  5. Receptores río abajo.

1.
  1.
    1. Oferta del licenciante – Material con licencia. Cada beneficiario de la licencia automáticamente recibe una oferta del licenciatario para ejercer los derechos con licencia bajo los términos y condiciones de esta licencia pública.
    2. Adicional ofrecemos del Licenciatario – Material adaptado. Cada destinatario de Material adaptado de usted automáticamente recibe una oferta del licenciatario para ejercer los derechos de licencia en el Material adaptado en las condiciones de licencia que del adaptador se aplican.
    3. Sin restricciones de aguas abajo. No puede ofrecer o imponer condiciones ni términos diferentes o adicionales o realizar ninguna medida tecnológica vigente, el Material con licencia si haciendo así que restringe el ejercicio de los derechos de licencia por cualquier destinatario del Material licenciado.
  2. No endoso. Nada en esta licencia pública constituye o puede interpretarse como un permiso para afirmar o implicar que eres, o que su uso del Material con licencia es, conectado con, patrocinado, respaldado o estatus oficial por el licenciante u otros designan para recibir reconocimiento conforme a lo dispuesto en la sección 3(a)(1)(A)(i).
2. **Otros derechos**.

1.
  1. Los derechos morales, como el derecho de integridad, no están autorizados bajo esta licencia pública, ni tampoco publicidad, privacidad y otros derechos similares; sin embargo, a la medida de lo posible, el Licenciante renuncia y acuerda no hacer valer ninguno de dichos derechos sostenido por el licenciante en la medida necesaria para que pueda ejercitar los derechos de licencia, pero no.
  2. Los derechos de patentes y marcas no están autorizados bajo esta licencia pública.
  3. La medida de lo posible, el Licenciante renuncia a cualquier derecho a cobrar derechos de autor de usted para el ejercicio de los derechos de licencia, si directamente o a través de una sociedad de gestión colectiva bajo cualquier esquema licencia legal u obligatorio voluntario o renunciable. En los demás casos el licenciante expresamente reserva cualquier derecho a cobrar esos derechos de autor.

**Sección 3 – condiciones de la licencia.**

El ejercicio de los derechos de licencia se hace expresamente las siguientes condiciones.

1. **Atribución**.
  1. Si compartes el Material con licencia (incluyendo en forma modificada), usted debe:
    1. retener los siguientes si es suministrada por el licenciante con el Material bajo licencia:
      1. identificación del creador del Material con licencia y cualesquiera otras designadas para recibir reconocimiento, de ninguna manera razonable solicitada por el licenciante (incluyendo seudónimo si señalado);
      2. un aviso de copyright;
      3. un aviso que se refiere a esta licencia pública;
      4. un aviso que se refiere a la exclusión de garantías;
      5. un URI o el hipervínculo para el Material bajo licencia en la medida posible;
    2. indicar si usted modifica el Material con licencia y retener una indicación de las modificaciones anteriores; y
    3. indicar que el Material con licencia está licenciado bajo esta licencia pública e incluyen el texto, o el URI o el hipervínculo de esta licencia pública.
  2. Usted puede cumplir con que las condiciones en la sección 3(a)(1) de cualquier manera razonable basan en el medio, medio y contexto en el que compartir el Material con licencia. Por ejemplo, puede ser razonable satisfacer las condiciones proporcionando un URI o un hipervínculo a un recurso que incluye la información requerida.
  3. Si solicitado por el licenciante, debe quitar cualquier información requerida por la sección 3(a)(1)(A) en la medida que resulten razonablemente practicable.
2. **Compartir Igual**.

Además de las condiciones en el apartado 3, si puedes compartir adaptado Material produce, aplicarán las siguientes condiciones también.

1.
  1. Licencia del adaptador se aplica debe ser una licencia de Creative Commons con los mismos elementos de licencia, esta versión o posterior, o una licencia Compatible BY-SA.
  2. Debe incluir el texto, o el URI o el hipervínculo a, el adaptador licencia aplicas. Puede cumplir esta condición en cualquier forma razonable en el medio, medio y contexto en que se parte adaptado el Material base.
  3. No podrá ofrecer o imponer condiciones ni términos adicionales o diferentes ni aplicar ninguna medida tecnológica vigente para, Material adaptado que restringen el ejercicio de los derechos concedidos bajo licencia que los adaptadores se aplican.

**Sección 4 – derechos de Sui Generis la base de datos.**

Donde los derechos de licencia incluyen Sui Generis derechos de base de datos que se aplican a su uso del Material con licencia:

1. para evitar toda duda, sección 2(a)(1) le otorga el derecho a extracto, reutilizar, reproducir y compartir todos o una parte sustancial de los contenidos de la base de datos;
2. Si se incluyen todos o una parte sustancial de los contenidos de la base de datos en una base de datos en la que tiene Sui Generis de derechos de base de datos, entonces la base de datos en la que usted tiene derechos de base de datos de Sui Generis (pero no su contenido individual) es Material adaptado, incluyendo fines de sección 3 (b); y
3. Usted debe cumplir con las condiciones en la sección a si compartes todo o una parte sustancial de los contenidos de la base de datos.

Para evitar dudas, esta sección 4 complementa y no reemplaza sus obligaciones bajo esta licencia pública donde los derechos de licencia incluyen otros derechos de autor y derechos similares.

**Sección 5 – exclusión de garantías y limitación de responsabilidad.**

1. **Si no se otra manera por separado por el licenciante, en la medida posible, el Licenciante ofrece Material con licencia como-es y como disponible y no hace representaciones o garantías de ningún tipo sobre el Material con licencia, ya sea expresa, IMPLÍCITA, estatutaria o de otro. Esto incluye, sin limitación, garantías de título, COMERCIABILIDAD, idoneidad para un propósito particular, no infracción, ausencia de defectos latentes o de otros, exactitud o la presencia o ausencia de errores, sean o no conocidos o detectable. Donde renuncias de garantías no están permitidos en completo o en parte, esta exclusión puede no aplicarse a usted.**
2. **La medida de lo posible, en ningún caso el licenciante será responsable ante usted de cualquier teoría legal (incluyendo, sin limitación, negligencia) o de cualquier directas, especiales, indirectas, INCIDENTALES, CONSECUENTES, punitivas, ejemplares u otras pérdidas, costos, gastos o daños que surjan de esta licencia pública o uso de los materiales bajo licencia, incluso si el licenciante ha sido advertido de la posibilidad de tales pérdidas, costos, gastos o daños y perjuicios. Cuando una limitación de responsabilidad no está permitida en completo o en parte, esta limitación puede no aplicarse a usted.**

1. La exclusión de garantías y limitación de responsabilidad anteriormente indicada podrá ser interpretados de manera que, a la medida de lo posible, más cerca se aproxima a una negación absoluta y renuncia a toda responsabilidad.

**Sección 6 – término y terminación.**

1. Esta licencia pública se aplica para el término del derecho de autor y derechos similares con licencia aquí. Sin embargo, si usted no cumple con esta licencia pública, entonces sus derechos bajo esta licencia pública terminan automáticamente.
2. Cuando ha terminado su derecho a utilizar el Material licenciado bajo la sección 6, proceda a reintegrar:
  1. automáticamente a partir de la fecha de la violación está curado, siempre se cura dentro de 30 días de su descubrimiento de la violación; o
  2. sobre expreso reintegro por el licenciante.

Para evitar dudas, esta sección 6.b no afecta a ningún derecho que el licenciante tenga que buscar remedios para la violación de esta licencia pública.

1. Para evitar dudas, el licenciante también puede ofrecer el Material licenciado bajo los términos o condiciones o dejar de distribuir el Material bajo licencia en cualquier momento; sin embargo, hacerlo así no terminará esta licencia pública.
2. Secciones 1, 5, 6, 7y 8 sobrevivirán la terminación de esta licencia pública.

**Sección 7 – otros términos y condiciones.**

1. El licenciante no estará obligado por los términos adicionales o diferentes o condiciones comunicadas por usted a menos que se acuerde expresamente.
2. Acuerdos, entendimientos ni acuerdos sobre el Material con licencia no indicadas en este documento son separados e independientes de los términos y condiciones de esta licencia pública.

**Sección 8 – interpretación.**

1. Para evitar dudas, esta licencia pública no y no podrá ser interpretada para, reducir, limitar, restringir o imponer condiciones a cualquier uso del Material con licencia que legalmente podría realizarse sin el permiso bajo esta licencia pública.
2. La medida de lo posible, si alguna disposición de esta licencia pública se considera inaplicable, se será automáticamente reformar a la medida mínima necesaria para hacerla ejecutable. Si la prestación no puede ser reformada, será separado de esta licencia pública sin afectar la exigibilidad de los restantes términos y condiciones.
3. Ningún término o condición de esta licencia pública se aplicará y no incumplimiento consentido a menos que expresamente por el licenciante.
4. Nada en esta licencia pública constituye o puede ser interpretada como una limitación sobre, o renuncia de cualquier privilegios e inmunidades que se aplican al licenciante o usted, incluyendo de los procesos legales de cualquier jurisdicción o autoridad.