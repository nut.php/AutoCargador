<?php

namespace Nut;

/**
 * Permite definir uno o varios directorios donde buscar un archivos de clase
 * para un espacio de nombre o una clase especifica, asociando una función en
 * la cola de __autoload proporcionada por spl.
 *
 * @author Neder Alfonso Fandiño Andrade <neafan@hotmail.com en nekoos.com>
 */
class AutoCargador {

  /**
   * Atributo destinado a alamacenar la lista de configuraciones del usuario.
   * @var array
   */
  private static $configuraciones;

  /**
   * Describe el estilo de escritura entre mayusculas y minusculas.
   * @var callable
   */
  private static $formatoDirectorio;

  /**
   * Almacena la ruta asociada al directorio raiz del proyecto.
   * @var rutaRaiz
   */
  private static $rutaRaiz;

  /**
   * Método modificador el atributo rutaRaiz
   * @param string $cadena
   */
  public static function modificarRutaRaiz(string $cadena) {
    self::$rutaRaiz = $cadena;
  }

  /**
   * Método modificador del atributo formatoDirectorio
   * @param \Nut\callable $funcion
   */
  public static function modificarFormatoDirectorios(callable $funcion) {
    self::$formatoDirectorio = $funcion;
  }

  /**
   * Permite cargar la configuracion asignada por el usuario desde diferentes
   * origenes tales como un fichero, objeto o arreglo, e inicializa los atributos
   * de la clase con valores por defecto en caso de ser necesario.
   * @staticvar int $ejecucion
   * @param string|array|object $archivo
   * @return \self
   */
  public static function cargarConfiguracion($archivo) {
    static $ejecucion = 0;
    if (!$ejecucion++) {
      if (is_string($archivo) and file_exists($archivo)) {
        self::$configuraciones = self::cargarArchivo($archivo);
      } elseif (is_array($archivo)) {
        self::$configuraciones = $archivo;
      } elseif (is_object($archivo)) {
        self::$configuraciones = json_decode(json_encode($archivo), TRUE);
      }
      self::registrarAutoCargas();
      self::$formatoDirectorio = self::$formatoDirectorio ??
          function ($subespacio) {
        return strtolower($subespacio);
      };
    } else {
      exit('La carga de configuracion en AutoCargador debe ser ejecutado una única vez.');
    }
    return new self;
  }

  /**
   * Registra una funcion en la pila de __autoload con la implementacion
   * necesaria para cumplir la definicion establecida por el usuario.
   */
  private static function registrarAutoCargas() {
    self::$configuraciones['clases'] = self::$configuraciones['clases'] ?? [];
    foreach (self::$configuraciones['clases'] as $clase => $directorio) {
      spl_autoload_register(function ($criterio) use ($clase, $directorio) {
        self::cargarRutaPorClase($criterio, $clase, $directorio);
      });
    }
    self::$configuraciones['espacios'] = self::$configuraciones['espacios'] ?? [];
    foreach (self::$configuraciones['espacios'] as $espacio => $directorios) {
      spl_autoload_register(function ($criterio) use ($espacio, $directorios) {
        self::cargarRutasPorEspacio($criterio, $espacio, $directorios);
      });
    }
  }

  /**
   * Describe una procedimiento en función al espacio y directorios definidos.
   * @param string $criterio
   * @param string $espacio
   * @param string|array $directorios
   */
  private static function cargarRutasPorEspacio($criterio, $espacio, $directorios) {
    $coincidencias = [];
    $espacioReal = preg_quote(str_replace('.', '\\', $espacio));
    $patron = "/^(?:\\\)?($espacioReal\\\?(.*)?\\\([^\\\]+$))/i";
    $esValido = preg_match($patron, $criterio, $coincidencias);
    if (is_string($directorios)) {
      $directorios = [$directorios];
    }
    if ($esValido) {
      foreach ($directorios as $directorio) {
        $ruta = implode(DIRECTORY_SEPARATOR, array_filter([
                self::$rutaRaiz, //Raiz
                $directorio, //Directorio asociado al espacio de nombre
                (self::$formatoDirectorio)($coincidencias[2]), //Directorio asociado al subespacio de nombre
                $coincidencias[3] //Nombre de la clase
            ])) . ".php";
        is_readable($ruta) and include_once $ruta;
        unset(self::$configuraciones['espacios'][$espacio]);
      }
    }
  }

  /**
   * Describe una procedimiento en función a la clase y directorio definido.
   * @param string $criterio
   * @param string $clase
   * @param string $directorio
   */
  private static function cargarRutaPorClase($criterio, $clase, $directorio) {
    $claseReal = str_replace('.', '\\', $clase);
    $esValido = ($criterio == $claseReal);
    if ($esValido) {
      $ruta = "$directorio.php";
      is_readable($ruta) and include_once $ruta;
      unset(self::$configuraciones['clases'][$clase]);
    }
  }

  private static function cargarArchivo($archivo) {
    $extension = pathinfo($archivo, PATHINFO_EXTENSION);
    if ($extension == 'json') {
      return json_decode(file_get_contents($archivo), TRUE);
    }
    return include_once $archivo;
  }

}
